Note 2018-07-12: This plugin still seems to work from the wordpress perspective
as of Wordpress 4.9.7. However the Hubzilla compainion side up seems partially
broken, see: https://framagit.org/hubzilla/addons

=== Hubzilla_WP ===

Contributors: duthied, macgirvin

Donate link: TBD

Tags: hubzilla, crosspost

Requires at least: 3.2

Tested up to: 3.2 (Seems to work with 4.9.7 as well)

Stable tag: 1.3

CrossPost to Hubzilla for WordPress

== Description ==

This plugin allows you to cross post to your Hubzilla account.

**Note, this plugin converts the html from wordpress into bbcode.  
The bbcode conversion doesn't handle height and width of an image, so be warned that if you post a very large
image that is resized via height and with attributes, those attributes won't be honored in the resulatant post.

== Changelog ==

= 1.3 = 

* post WP comments as well as posts

= 1.2 =

* ensured no function name collision

= 1.1 =

* Added feedback to settings form post.

= 1.0 =

* Initial release.

== Installation ==

1. Install the plugin from your Wordpress admin panel.

OR

1. Upload the plugin folder to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. On the settings page enter your account name and password and the nickname of the channel to post to -  then click the submit button
4. To cross-post ensure the check box in the 'Cross Post To hubzilla' is checked before publishing.

== Frequently Asked Questions ==

1. Where can I find the original author's release?

* https://macgirvin.com/cloud/mike

2. Is there a similar plugin for GNU Social?

* Maybe, see: https://wordpress.org/plugins/wp-gnusocial/